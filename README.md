# **README**

___

#### Liste Exercices Révisions:

- <div>
  <a href="./Fichiers/LER/LE1 - FoncNat.md">LE1 - Fonctions Natives</a>
  </div>

- <div>
  <a href="./Fichiers/LER/LE2 - Cond.md">LE2 - Conditions</a>
  </div>

- <div>
  <a href="./Fichiers/LER/LE3 - Boucles.md">LE2 - Boucles</a>
  </div>

#### Liste Exercices Entrenement:

- <div>
  <a href="./Fichiers/LE/LE123.md">LE1/LE2/LE3</a>
  </div>

#### Liste Gros Exercices:

- <div>
  <a href="./Fichiers/EC/Jeu Plus ou Moins.md">Jeu du plus ou moins</a>
  </div>
