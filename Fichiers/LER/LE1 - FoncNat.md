# Exercices Fonctions Natives

___

## **Rappels**

- > #### Fonctions natives :
  > 
  > | Fonction   | Utilité                                                    |
  > |:----------:|:----------------------------------------------------------:|
  > | int()      | Transformer un str en int                                  |
  > | str()      | Transformer un int en str                                  |
  > | type(var)  | Savoir si la variable "var" est de quel type (str, int...) |
  > | print(var) | Affiche dans la console la variable "var"                  |
  > 
  > #### Exemples :
  > 
  > ```python
  > var = int("84") # la variable var contiendra 84
  > var2 = str(5) # la variable var2 contiendra "5"
  > 
  > #-------------------------------------------------#
  > 
  > 
  > var = "maison"
  > type(var) # le type de var est str donc type(var) renvoie str
  > print(var) # print va afficher la variable var dans la console c'est a dire maison
  > ```

___

## **Ecercices**

### Exercice 1:

#### Terminer la fonction transforme_str(nombre) en renvoyant le str du nombre en paramètre.

```python
def transforme_str(nombre:int):
    nombre_en_str = .....
    return nombre_en_str
```

### Exercice 2:

#### Compléter la fonction affiche_nombre(chaine) pour que la fonction affiche la chaine en paramètre.

```python
def affiche_nombre(chaine:str):
    .....
```

## Exercice 3:

#### Compléter la fonction affiche_type(variable) pour que la fonction affiche le type de la variable en paramètre.

```python
def affiche_type(variable):
    type_variable = .....
    print(type_variable)
```

___

## **Exercices**

### Exercice 1:

#### Créer un programme trans_int(chaine) qui prend un nombre en chaîne de caractères en paramètre, le convertit en int et affiche son double.

```python
>>> trans_int("52")
104
```

### Exercice 2:

#### Créer un programme qui accepte un nombre en paramètre et le convertit en chaîne de caractères.

```python
>>> var = 60
>>> trans_str(var)
>>> type(var)
str
```
