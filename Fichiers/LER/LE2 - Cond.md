# Exercices Conditions

___

## **Rappels**

- > #### Fonctions :
  > 
  > | Fonction | Utilité                                                                                                    |
  > |:--------:|:----------------------------------------------------------------------------------------------------------:|
  > | if       | Si une condition est vraie alors le programme dans le if va s'exécuter                                     |
  > | elif     | Si le if ne c'est pas exécuter alors, si la condition du elif est vraie le programme du elif va s'exécuter |
  > | else     | Si tous les if et les elif n'ont pas était exécuter alors le programme du else va s'exécuter               |
  > 
  > #### Exemples :
  > 
  > ```python
  > def test(nombre:int):
  >     if nombre > 0:
  >         print("plus grand que 0")
  >     elif nombre < 0:
  >         print("plus petit que 0")
  >     else:
  >         print("le nombre est 0")
  > 
  > # test de la fonction
  > #1)
  > >>> test(20)
  > plus grand que 0 # car la condition du if est vraie
  > #2)
  > >>> test(-15)
  > plus petit que 0 # car la condition du if est fausse donc le programme test la condition du elif est vu qu'elle est vraie il exécute son programme
  > #3)
  > >>> test(0)
  > le nombre est 0 # car la condition du if et du elif sont fausses donc c'est le programme du else qui s'exécute
  > ```
- > #### Conditions :
  > 
  > | Condition | Comparaison                    |
  > |:---------:|:------------------------------:|
  > | ==        | A égal B                       |
  > | !=        | A different de B               |
  > | <         | A strictement plus petit que B |
  > | <=        | A plus petit qu B              |
  > | >         | A strictement plus grand que B |
  > | >=        | A plus grand que B             |
  > 
  > #### Exemples :
  > 
  > ```python
  > 5 == 5 # renvoie True
  > 5 != 5 # renvoie False
  > 15 < 15 # renvoie False
  > 15 <= 15 # renvoie True
  > 32 > 32 # renvoie False
  > 32 >= 32 # renvoie True
  > 
  > # fonction aussi avec des str
  > 
  > 5 == "maison" #renvoie False
  > 15 != "maison" # renvoie True
  > 
  > # fonction aussi avec la fonction native type()
  > # type(5) = int
  > # type("maison") = str
  > 
  > type(5) == int # renvoie True
  > type(84) != str # renvoie True
  > ```
- > #### Opérateurs :
  > 
  > | Opérateur | Explication                                                  |
  > |:---------:|:------------------------------------------------------------:|
  > | or        | La condition sera vraie si l'une des conditions est vraie    |
  > | and       | La condition sera vraie si toutes les conditions sont vraies |
  > | not       | la condition sera vraie si la condition est fausse           |
  > 
  > #### Exemples :
  > 
  > ```python
  > 5 > 2 or 84 == 56 # renvoie True car la condition 5 > 2 est vraie
  > 5 > 2 and 84 != 56 # renvoie True car les deux conditions sont vraies
  > not (5 < 2) # renvoie True car 5 < 2 est faux
  > ```

___

## **Ecercices**

### Exercice 1:

#### Compléter la fonction affiche_plus_grand(nombre1, nombre2) pour que le plus grand nombre des deux soit affiché.

```python
def affiche_plus_grand(nombre1, nombre2):
    if ..... :
        print(nombre1)
    else:
        print(nombre2)
```

### Exercice 2:

#### Compléter la fonction mutiplication3(nb1, nb2, nb3) qui fait la multiplication des trois nombres seulement si nb1 strictement inférieur à nb2 et que nb3 soit inférieur soit égal a 10.

```python
def multiplication3(nb1, nb2, nb3):
    if ..... :
        return nb1 * nb2 * nb3
    else:
        print("ne respecte pas les règles")
```

### Exercice 3:

#### Completer la fonction assemblage_mot(mot1, mot2) qui fait la concatenation des deux mots si les deux mots sont bien du type str.

```python
def assemblage_mot(mot1, mot2):
    if type(.....) == ..... and ..... :
        return mot1 + mot2
    else:
        print("l'un des mots n'est pas une chaine de caractère")
```

___

## **Exercices**

### Exercice 1:

#### Faire un programme moitie_double(nbr) qui prend un nombre en paramètre et que si le nombre est pair alors le programme renvoie la moitié de ce nombre, sinon il renvoie le double.

```python
>>> moitie_double(4)
2
>>> moitie_double(7)
14
```

### Exercice 2:

#### Faire un programme phrase_type(var) qui a une variable en paramètre et qui écrit une phrase différente suivant le type de la variable.

```python
>>> phrase_type(5)
c'est un nombre
>>> phrase_type("maison")
c'est un mot
```

### Exercice 3:

#### Faire un programme melange(nb1, nb2) qui prend deux nombres en paramètre et que :

- #### si les deux nombres sont égaux le programme renvoie leur addition

- #### sinon si le premier est supérieur strictement au second alors le programme renvoie leur différence

- #### sinon si le premier est supérieur strictement à 0 et que le second aussi alors le programme renvoie leur multiplication

- #### sinon le programme renvoie leur division

```python
>>> melange(2, 2) # 2 + 2
4
>>> melange(8, 4) # 8 - 4
4
>>> melange(6, 10) # 6 * 10
60
>>> melange(-2, 5) # -2 / 5
-0.4
```
