# Exercices Boucle

___

## **Rappels**

- > #### Fonctions :
  > 
  > | Fonction | Description                                                                                                          | Utilisation                                     |
  > |:--------:|:--------------------------------------------------------------------------------------------------------------------:|:-----------------------------------------------:|
  > | len()    | Permet de savoir la taille d'une chaine ou d'une liste.                                                              | len(var)                                        |
  > | for      | Permets de faire un nombre de tours précis, ou de parcourir un itérable. La boucle a un programme qui lui est propre | for var1 in range(var2) **ou** for var1 in var2 |
  > | while    | Tourne en boucle tant que la condition n'est pas vraie "True", avec un programme qui lui est propre                  | while condition                                 |
  > 
  > #### Exemples :
  > 
  > ```python
  > mot = "maison"
  > liste = [1,6,4,8]
  > print(len(mot)) # cela affichera 6 car il y a 6 lettres dans maison
  > print(len(liste)) # cela affichera 4 car il y a 4 valeurs dans la liste
  >
  >
  > # Exemples boucle for /tester dans Thonny/
  > # Avec range
  > for val in range(8):
  >     print(val)
  >
  > for val in range(4,9):
  >     print(val)
  >
  > for val in range(51, 100, 10):
  >     print(val)
  >
  > # boucle for avec des variables
  > mot = "programme"
  > for indice in range(len(mot)): # parcours par indices
  >     print(mot[indice])
  >
  > liste = [1,5,4,0]
  > for indice in range(len(mot)): # parcours par indices
  >     print(liste[indice])
  >
  > # Sans range
  > mot = "Exemple"
  > for lettre in mot: # parcours par valeurs
  >     print(lettre)
  >
  > liste = ["b","o","n","j","o","u","r"]
  > for valeur in liste: # parcours par valeurs
  >     print(valeur, end="")
  >
  >
  > # Exemples boucle while /tester dans Thonny/
  > nb1 = 10
  > nb2 = 0
  > while nb2 < nb1:
  >     nb2 = nb2 + 1
  >     print(nb2)
  >
  > nb1 = 154
  > nb2 = 0
  > while nb1 // nb2 > nb2 or nb2 < 10:
  >     nb2 = nb2 + 1
  >     print(nb1,  " / ", nb2, " = ", nb1 // nb2)
  >
  > # Jouer avec des variables
  > mot1 = "casino" 
  > mot2 = ""
  > while len(mot2) != len(mot1):
  >     mot2 = mot2 + mot1[len(mot1) - 1 - len(mot2)]
  >     print(mot2)
  > print("l'inverse de", mot1, "est", mot2)  
  > ```
  ___
  ### Exercices
  ### Exercice 1:
  #### Compléter le programme affiche_lettre(mot:str) qui affiche les lettres du mot une par une.
  ```python
  def affiche_lettre(mot:str) -> None:
      for ..... :
           print(.....)
  ```  
  ### Exercice 2:
  #### Compléter le programme nombre_lettre(mot:str) -> int qui renvoie le nombre de lettres dans le mot.
  ```python
  def nombre_lettre(mot:str) -> int:
      return .....
  ```
  ___
  ### Exercice
  ### Exercice 1
  #### Partie 1
  #### Faire un programme inverse_mot(mot:str) -> str qui renvoie le mot inverse de celui en paramètre.
  #### Exemple
  ```python
  >>> inverse_mot("programme")
  emmeargorp
  ```
  #### Partie 2
  #### En utilisant le programme de la partie 1 faire un programme inverse_impair(mot:str) -> str qui renvoie le mot inverse de le celui en paramètre si le nombre de lettres dans le mot est impair sinon il renvoie le mot de base.
  #### Exemple
  ```python
  >>> inverse_impair("test")
  test
  >>> inverse_impair("nsi")
  isn
  ```
  ### Exercice 2
  #### Partie 1
  #### Faire un programme est_voyelle(lettre:str) -> bool qui revoie True ou False suivant si la lettre en paramètre est une voyelle.
  #### Exemple
  ```python
  >>> est_voyelle("o")
  True
  >>> est_voyelle("l")
  False
  ```
  #### Partie 2
  #### En utilisant le programme de la partie 1 faire un programme renvoie_voyelle(mot:str) -> str qui renvoie l'ensemble des voyelles du mot.
  #### Exemple
  ```python
  >>> renvoie_voyelle("casino") 
  aio
  >>> renvoie_voyelle("ecole")
  eoe
  ```