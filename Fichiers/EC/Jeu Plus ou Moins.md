# Jeu Plus ou Moins

___

## Consigne:

#### Completer les fonctions pour réaliser le jeu du plus ou moins.

```python
from random import randint

def plus_petit(nombre1:int, nombre2:int) -> bool:
    """
    Renvoie True si le nombre1 est plus petit que nombre2 sinon False
    
    parametres:
    nombre1 : (int) premier nombre
    nombre2 : (int) deuxieme nombre
    
    return:
    (bool) : nombre1 plus petit que nombre2
    """


def plus_ou_moins(None) -> None:
    nombre_a_trouver = randint(0,100) # nombre à trouver généré aléatoirement
    
    nombre_utilisateur = int(input("Mettre un nombre : ")) # demander au joueur d'entrer un nombre
    
    while ..... : # boucle du jeu
        """
        tester si le nombre du joueur est plus petit que le nombre à trouver, lui dire et lui demander d'entrer un nouveau nombre
        """
    
    print("Bravo, le nombre est bien, ", nombre_a_trouver) # fin du jeu
```


